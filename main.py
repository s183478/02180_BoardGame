# Source https://www.geeksforgeeks.org/2048-game-in-python/

# 2048.py 
  
# importing the logic.py file 
# where we have written all the 
# logic functions used. 
import logic
import astar_ai as ai 
  

## Our pretty print
def printBoardPretty(mat):
    for i in range(0,4):
        print(mat[i]) 

def start_game(max_depth):
    # Driver code 
    if __name__ == '__main__':  
        mat = logic.start_game()
    move_count = 0
    while(True): 
        # taking the user input 
        # for next step
        try:
            x = ai.astar_search(mat, max_depth) 
            move_count += 1
            print("Moves used: " + str(move_count))
        except:
            print("I have lost the game")
            # printBoardPretty(mat)
            return move_count
            break
      
        # we have to move up 
        if(x == 'W' or x == 'w'): 
      
            # call the move_up funtion 
            mat, flag = logic.move_up(mat) 
      
            # get the current state and print it 
            status = logic.get_current_state(mat)
            print(status) 
      
            # if game not ove then continue 
            # and add a new two 
            if(status == 'GAME NOT OVER'): 
                logic.add_new_2(mat, 1) 
      
            # else break the loop  
            else: 
                break
      
        # the above process will be followed 
        # in case of each type of move 
        # below 
      
        # to move down 
        elif(x == 'S' or x == 's'): 
            mat, flag = logic.move_down(mat) 
            status = logic.get_current_state(mat) 
            print(status) 
            if(status == 'GAME NOT OVER'): 
                logic.add_new_2(mat, 2) 
            else: 
                break
      
        # to move left 
        elif(x == 'A' or x == 'a'): 
            mat, flag = logic.move_left(mat) 
            status = logic.get_current_state(mat) 
            print(status) 
            if(status == 'GAME NOT OVER'): 
                logic.add_new_2(mat, 2) 
            else: 
                break
      
        # to move right 
        elif(x == 'D' or x == 'd'): 
            mat, flag = logic.move_right(mat) 
            status = logic.get_current_state(mat) 
            print(status) 
            if(status == 'GAME NOT OVER'): 
                logic.add_new_2(mat, 2) 
            else: 
                break
        else: 
            print("Invalid Key Pressed") 
      
        # print the matrix after each 
        # move. 
        printBoardPretty(mat)
start_game(2)
# for depth in range(1,4):
#     move_count = 0
#     for count in range(0,50):
#         move_count += start_game(depth)
#     move_count = move_count/50
#     print("Average moves used for game with depth " + str(depth) + ": " + str(move_count))
# print("Done")
