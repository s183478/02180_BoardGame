# -*- coding: utf-8 -*-
import logic
import sys

mat = logic.start_game()


def printBoardPretty(mat):
    for i in range(0, 4):
        print(mat[i])

def smoothness_cost(mat):
    value = 0
    for i in range(0, 4):
        for j in range(0, 4):
            if not(i-1 < 0) and mat[i-1][j] != 0 and mat[i-1][j] < mat[i][j]:
                value += mat[i-1][j] + mat[i][j] * 2
            if not(i+1 >= 4) and mat[i+1][j] != 0 and mat[i+1][j] < mat[i][j]:
                value += mat[i+1][j] + mat[i][j] * 2
            if not(j-1 < 0) and mat[i][j-1] != 0 and mat[i][j-1] < mat[i][j]:
                value += mat[i][j-1] + mat[i][j] * 2
            if not(j+1 >= 0) and mat[i][j+1] != 0 and mat[i][j+1] < mat[i][j]:
                value += mat[i][j+1] + mat[i][j] * 2
    # print("smoothness cost: " + str(value))            
    return value

def empty_squares_cost(mat):
    # Returns the count of empty squares in the matrix times 100
    mat_sum = 0
    for i in range(0, 4):
        for j in range(0, 4):
            if mat[i][j] == 0:
                mat_sum += 1000

    return mat_sum

def squared_sum_cost(mat):
    sum_value = 0
    for i in range(0, 4):
        for j in range(0, 4):
            sum_value += mat[i][j]**2
    return sum_value

def monotonicity_cost(mat):
    left_to_right = 0
    down_to_top = 0
    cost_value = 200
    
    #left to right
    for i in range(0, 4):
        for j in range(0, 3):
            if mat[i][j] < mat[i][j+1]:
                left_to_right += cost_value
            
            if mat[i][j] == mat[i][j+1]/2:
                left_to_right += cost_value
    
    #down to top
    for i in range(3,0,-1):
        for j in range(0 ,4):
            if mat[i][j] < mat[i-1][j]:
                down_to_top += cost_value
            if mat[i][j] == mat[i-1][j]/2:
                down_to_top += cost_value
    return left_to_right + down_to_top

def f_cost(mat):
    return smoothness_cost(mat) + monotonicity_cost(mat)
    
def get_moves(mat):
    possibleMoves = [logic.move_left, logic.move_right,logic.move_up, logic.move_down]
    output = []
    directions = ['a','d','w','s']

    for i in range(0,4):
        moveMat, moveChanged = possibleMoves[i](mat)
        # print((moveMat, moveChanged, directions[i]))
        if moveChanged:
            output.append((moveMat, directions[i]))

    return output


def astar_search(mat, max_depth):
    frontier = []
    for move in get_moves(mat):
        frontier.append(move)
        
    current = ([],'')
    while len(frontier) != 0 and len(current[1]) < max_depth:
        #Find node with lowest f-cost
        min_fcost = sys.maxsize
        index = 0
        for i in range(0,len(frontier)):
            node_cost = f_cost((frontier[i])[0])
            if node_cost < min_fcost:
                min_fcost = node_cost
                current = frontier[i]
                index = i
                
        # Check if current selection is goal
        for i in range(0,4):
            for j in range(0,4):
                if (current[0])[i][j] == 2048:
                    return (current[1])[0]
        
        
        del frontier[index]
        for move in get_moves(current[0]):
            current_path = current[1] + move[1]
            frontier.append((move[0],current_path))
    #Exited out of while-loop, find the lowest f-cost
    min_fcost = sys.maxsize
    for i in range(0,len(frontier)):
        node_cost = f_cost((frontier[i])[0])
        if node_cost < min_fcost:
            min_fcost = node_cost
            current = frontier[i]
    # print("Printing current: " + current[1])
    return (current[1])[0]
